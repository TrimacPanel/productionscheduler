package controller;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import model.OpenWorkOrder;
import model.ProductionSchedule;

public class EditScreenController {

    OpenWorkOrder selectedWorkOrder;
    MainScreenController mainScreenController;

    @FXML
    private Label titleLabel;

    @FXML
    private TextField prioritySeqTxtID;

    @FXML
    private DatePicker schDateDatePickerID;

    @FXML
    private ComboBox<String> prodLineDropDownID;

    @FXML
    private ComboBox<String> workShiftDropDownID;

    @FXML
    private Button saveButtonID;

    @FXML
    private Button cancelButtonID;

    @FXML
    void onActionCancelButton(ActionEvent event) {
        Stage editWindow = (Stage) ((Button) event.getSource()).getScene().getWindow();
        editWindow.close();
    }

    @FXML
    void onActionSaveButton(ActionEvent event) {
        ObservableList<OpenWorkOrder> prodSchWorkOrdersLocalCopy = ProductionSchedule.getProdSchWorkOrders();

        //Make change to object
        prodSchWorkOrdersLocalCopy.remove(selectedWorkOrder);

        selectedWorkOrder.setSchDate(java.sql.Date.valueOf(schDateDatePickerID.getValue()));
        selectedWorkOrder.setProdLine(prodLineDropDownID.getValue());
        selectedWorkOrder.setWorkShift(workShiftDropDownID.getValue());
        //TODO: Unhandled exception if user types alphabet
        selectedWorkOrder.setPrioritySeq(Integer.parseInt(prioritySeqTxtID.getText()));

        prodSchWorkOrdersLocalCopy.add(selectedWorkOrder);

        ProductionSchedule.setProdSchWorkOrders(prodSchWorkOrdersLocalCopy);
        Stage editWindow = (Stage) ((Button) event.getSource()).getScene().getWindow();

        //Refresh mainscreen table with changed data
        mainScreenController.editScreenSaveChanges(selectedWorkOrder);

        editWindow.close();
    }

    public void loadRowData(OpenWorkOrder rowData, MainScreenController mainScreenController) {
        this.mainScreenController = mainScreenController;

        //TODO: May want to change work order number to be larger than this label. Will have to introduce another label.
        titleLabel.setText("Editing Work Order: " + rowData.getWorkOrderNum());

        if (rowData.getSchDate() != null){
            schDateDatePickerID.setValue(rowData.getSchDate().toLocalDate());
        }

        prodLineDropDownID.getItems().removeAll(prodLineDropDownID.getItems());
        //TODO: Get correct production line data
        prodLineDropDownID.getItems().addAll("JAVON", "VORWOOD", "HARLEN 1");
        if (rowData.getProdLine() != null){
            prodLineDropDownID.getSelectionModel().select(rowData.getProdLine());
        }

        workShiftDropDownID.getItems().removeAll(workShiftDropDownID.getItems());
        workShiftDropDownID.getItems().addAll("DAY", "SWING");
        if (rowData.getWorkShift() != null){
            workShiftDropDownID.getSelectionModel().select(rowData.getWorkShift());
        }

        if (rowData.getPrioritySeq() != 0){
            //TODO: Research a better control for only integers. Or see if you can lockdown a textbox for integers.
            prioritySeqTxtID.setText(Integer.toString(rowData.getPrioritySeq()));
        }

        selectedWorkOrder = rowData;
    }
}
