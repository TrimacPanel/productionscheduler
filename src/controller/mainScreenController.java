package controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.skin.TableHeaderRow;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.OpenWorkOrder;
import model.ProductionSchedule;
import model.WorkOrder;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;

public class MainScreenController implements Initializable {

    @FXML
    private TableView<OpenWorkOrder> prodSchTableView;

    @FXML
    private TableColumn<OpenWorkOrder, String> prodLineCol;

    @FXML
    private TableColumn<OpenWorkOrder, Date> schDateCol;

    @FXML
    private TableColumn<OpenWorkOrder, Integer> seqCol;

    @FXML
    private TableColumn<OpenWorkOrder, String> commentColID;

    @FXML
    private TableColumn<OpenWorkOrder, Date> dueDateCol;

    @FXML
    private TableColumn<OpenWorkOrder, Integer> workOrderNumCol;

    @FXML
    private TableColumn<OpenWorkOrder, String> macRefCol;

    @FXML
    private TableColumn<OpenWorkOrder, Integer> qtyCol;

    @FXML
    private TableColumn<OpenWorkOrder, String> finishedSkuCol;

    @FXML
    private TableColumn<OpenWorkOrder, String> itemDescCol;

    @FXML
    private TableColumn<OpenWorkOrder, String> rawSkuCol;

    @FXML
    private TableColumn<OpenWorkOrder, String> filmSkuCol;


    @FXML
    private TableColumn<OpenWorkOrder, Integer> unitSizeCol;

    @FXML
    private TableColumn<OpenWorkOrder, Integer> salesOrderCol;

    @Override
    public void initialize(URL location, ResourceBundle resources){
        prodLineCol.setCellValueFactory(new PropertyValueFactory<OpenWorkOrder, String>("prodLine"));
        schDateCol.setCellValueFactory(new PropertyValueFactory<OpenWorkOrder, Date>("schDate"));
        seqCol.setCellValueFactory(new PropertyValueFactory<OpenWorkOrder, Integer>("prioritySeq"));
        commentColID.setCellValueFactory(new PropertyValueFactory<OpenWorkOrder, String>("comment"));
        dueDateCol.setCellValueFactory(new PropertyValueFactory<OpenWorkOrder, Date>("dueDate"));
        workOrderNumCol.setCellValueFactory(new PropertyValueFactory<OpenWorkOrder, Integer>("workOrderNum"));
        macRefCol.setCellValueFactory(new PropertyValueFactory<OpenWorkOrder, String>("macolaRef"));
        qtyCol.setCellValueFactory(new PropertyValueFactory<OpenWorkOrder,Integer>("qty"));
        finishedSkuCol.setCellValueFactory(new PropertyValueFactory<OpenWorkOrder, String>("finishedSKU"));
        itemDescCol.setCellValueFactory(new PropertyValueFactory<OpenWorkOrder, String>("itemDesc"));
        rawSkuCol.setCellValueFactory(new PropertyValueFactory<OpenWorkOrder, String>("subSKU"));
        filmSkuCol.setCellValueFactory(new PropertyValueFactory<OpenWorkOrder, String>("filmSKU"));
        unitSizeCol.setCellValueFactory(new PropertyValueFactory<OpenWorkOrder, Integer>("unitSize"));
        salesOrderCol.setCellValueFactory(new PropertyValueFactory<OpenWorkOrder, Integer>("salesOrder"));
        ProductionSchedule.refreshProductionScheduleData();
        prodSchTableView.getItems().setAll(ProductionSchedule.getProdSchWorkOrders());
        autoResizeColumns(prodSchTableView);

        //https://o7planning.org/en/11533/opening-a-new-window-in-javafx
        //Opening a new window
        prodSchTableView.setRowFactory(tv -> {
            TableRow<OpenWorkOrder> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    Stage primaryStage = (Stage) ((TableRow) event.getSource()).getScene().getWindow();

                    Scene secondScene = null;
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/editscreen.fxml"));
                    try {
                        secondScene = new Scene(loader.load());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // New window (Stage)
                    Stage newWindow = new Stage();
                    newWindow.setTitle("");
                    newWindow.setScene(secondScene);

                    // Specifies the modality for new window.
                    newWindow.initModality(Modality.WINDOW_MODAL);

                    // Specifies the owner Window (parent) for new window
                    newWindow.initOwner(primaryStage);

                    //Changes the decorations to be minimal but draggable
                    newWindow.initStyle(StageStyle.UTILITY);

                    //Edit popup window: Width is 400 and Height is 250

                    /*
                                        // Set position of second window, related to primary window.
                    The following is a simple center formula. First take the parent window width. Then divide it by 2.
                    Then you need to subtract the edit popup windows half of width.
                    The - 7, is a magic number that makes it looks more centered. I believe it's needed because the
                    primary window uses a "StageStyle" of default, which is a very decorative "more pixels around the outside"
                     */
                    int widthOfPrimaryWindow = (((int) primaryStage.getWidth()) - 7)/2;
                    widthOfPrimaryWindow = widthOfPrimaryWindow - 200;
                    int heightOfPrimaryWindow = (((int) primaryStage.getHeight()) - 7)/2;
                    heightOfPrimaryWindow = heightOfPrimaryWindow - 125;

                    newWindow.setX(primaryStage.getX() + widthOfPrimaryWindow); //Width
                    newWindow.setY(primaryStage.getY() + heightOfPrimaryWindow); //Height

                    //Populate data in the edit window, calls the load method in the controller class
                    OpenWorkOrder rowData = row.getItem();
                    EditScreenController controller = loader.getController();
                    controller.loadRowData(rowData, this);

                    newWindow.show();
                }
            });
            return row ;
        });

        //Disable dragging of columns or reordering of columns
        ObservableList<TableColumn<OpenWorkOrder, ?>> columns = prodSchTableView.getColumns();
        for (TableColumn<OpenWorkOrder, ?> iterator : columns){
            iterator.setReorderable(false);
        }
    }

    @FXML
    void onActionFileMenuClose(ActionEvent event) {
        System.exit(0);
    }

    @FXML
    void onActionRefreshButton(ActionEvent event) {
        ProductionSchedule.refreshProductionScheduleData();
        prodSchTableView.getItems().setAll(ProductionSchedule.getProdSchWorkOrders());
        autoResizeColumns(prodSchTableView);
     }

    @FXML
    void onKeyPressRefreshButton(KeyEvent event) {
        if (event.getCode() == KeyCode.F5) {
            ProductionSchedule.refreshProductionScheduleData();
        }
    }

    /*
    Taken from https://stackoverflow.com/questions/14650787/javafx-column-in-tableview-auto-fit-size
    Auto resizes the columns in the TableView by looking at the width of the data
     */
    public static void autoResizeColumns( TableView<?> table )
    {
        //Set the right policy
        table.setColumnResizePolicy( TableView.UNCONSTRAINED_RESIZE_POLICY);
        table.getColumns().stream().forEach( (column) ->
        {
            //Minimal width = columnheader
            Text t = new Text( column.getText() );
            double max = t.getLayoutBounds().getWidth();
            for ( int i = 0; i < table.getItems().size(); i++ )
            {
                //cell must not be empty
                if ( column.getCellData( i ) != null )
                {
                    t = new Text( column.getCellData( i ).toString() );
                    double calcwidth = t.getLayoutBounds().getWidth();
                    //remember new max-width
                    if ( calcwidth > max )
                    {
                        max = calcwidth;
                    }
                }
            }
            //set the new max-widht with some extra space
            column.setPrefWidth( max + 10.0d );
        } );
    }

    @FXML
    void onActionFilterButton(ActionEvent event) {
        //TODO: Implement filter button
    }

    @FXML
    void onActionSearchButton(ActionEvent event) {
        //TODO: Implement search button
    }

    @FXML
    void onActionPrintButton(ActionEvent event) {
        //TODO: Implement print button
    }

    public void editScreenSaveChanges(OpenWorkOrder workOrderNum) {
        ProductionSchedule.saveRowChanges(workOrderNum);
        prodSchTableView.getItems().setAll(ProductionSchedule.getProdSchWorkOrders());
        autoResizeColumns(prodSchTableView);
    }
}
