package model;

import java.sql.Date;

public class OpenWorkOrder extends WorkOrder{

    private Date schDate;
    private String prodLine;
    private String workShift;
    private String comment;
    private int prioritySeq;

    OpenWorkOrder(Date scheduledDate, String assignedProductionLine, String assignedWorkShift, int prioritySequence,
                  Date dueDate, int workOrderNumber, String macolaReference, int quantity, String finishedSKU, String itemDescription,
                  String substrateSKU, String filmSKU, int unitSize, String salesOrder, String comment){
        super(dueDate, workOrderNumber, macolaReference, quantity, finishedSKU, itemDescription, substrateSKU, filmSKU,
                unitSize, salesOrder);

        this.schDate = scheduledDate;
        this.prodLine = assignedProductionLine;
        this.workShift = assignedWorkShift;
        this.prioritySeq = prioritySequence;
        this.comment = comment;
    }

    public OpenWorkOrder(WorkOrder i) {
        super(i);

        this.schDate = null;
        this.prodLine = "";
        this.workShift = "";
        this.prioritySeq = 0;
        this.comment = "";
    }

    public Date getSchDate() {
        return schDate;
    }

    public void setSchDate(Date schDate) {
        this.schDate = schDate;
    }

    public String getProdLine() {
        return prodLine;
    }

    public void setProdLine(String prodLine) {
        this.prodLine = prodLine;
    }

    public String getWorkShift() {
        return workShift;
    }

    public void setWorkShift(String workShift) {
        this.workShift = workShift;
    }

    public int getPrioritySeq() {
        return prioritySeq;
    }

    public void setPrioritySeq(int prioritySeq) {
        this.prioritySeq = prioritySeq;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void convertWorkOrder(WorkOrder i) {
        super.setDueDate(i.getDueDate());
        super.setWorkOrderNum(i.getWorkOrderNum());
        super.setMacolaRef(i.getMacolaRef());
        super.setQty(i.getQty());
        super.setFinishedSKU(i.getFinishedSKU());
        super.setItemDesc(i.getItemDesc());
        super.setSubSKU(i.getSubSKU());
        super.setFilmSKU(i.getFilmSKU());
        super.setUnitSize(i.getUnitSize());
        super.setSalesOrder(i.getSalesOrder());
    }
}
