package model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.*;

public class ProductionSchedule {

    static final String productionScheduleConfig = "productionscheduleconfig";

    private static ObservableList<OpenWorkOrder> prodSchWorkOrders = FXCollections.observableArrayList();

    ProductionSchedule(){
        ObservableList<WorkOrder> macolaData = retrieveMacolaData(); //retrieve macola data
        ObservableList<OpenWorkOrder> productionScheduleData = retrieveProductionScheduleData(); //retrieve production schedule data

        // compare the two containers for, existence, status completion, and then deletion
        // Put processed data into prodSchWorkOrders
        prodSchWorkOrders = compareMacolaWithProductionScheduleData(macolaData, productionScheduleData);
        // assuming there is a instance method Class.getScore that returns int
        // (other implementations for comparator could be used too, of course)

        //FXCollections.sort(prodSchWorkOrders, new workOrderSortingComparator());
        //FXCollections.reverse(prodSchWorkOrders);
    }

    /*
    refreshes the production schedule data by checking the Macola data
     */
    public void refreshProductionScheduleData() {
        //Saves the production schedule OpenWorkOrders first, before doing a comparison and refreshing the data.
        if (ProductionSchedule.getProdSchWorkOrders().size() > 0){
            saveProductionScheduleData();
        }

        ObservableList<WorkOrder> macolaData = retrieveMacolaData(); //retrieve macola data
        ObservableList<OpenWorkOrder> productionScheduleData = retrieveProductionScheduleData(); //retrieve production schedule data

        // compare the two containers for, existence, status completion, and then deletion
        // Put processed data into prodSchWorkOrders
        prodSchWorkOrders = compareMacolaWithProductionScheduleData(macolaData, productionScheduleData);
    }

    /*
    compareMacolaWithProductionScheduleData is the logic for checking existence, status, and possible deleting data
    from the Production Schedule Data
     */
    private static ObservableList<OpenWorkOrder> compareMacolaWithProductionScheduleData(ObservableList<WorkOrder> macolaData, ObservableList<OpenWorkOrder> productionScheduleData){
        ObservableList<OpenWorkOrder> listOfItemsToDelete = FXCollections.observableArrayList();

        //Check for existence of Work Order Job
        for(WorkOrder i : macolaData){
            boolean exists = false;
            for(OpenWorkOrder j : productionScheduleData){
                if (i.getWorkOrderNum() == j.getWorkOrderNum()){
                    exists = true;

                    //Take macola work order data and add it to existing OpenWorkOrder
                    j.convertWorkOrder(i);
                }
            }

            if (exists == false){
                productionScheduleData.add(new OpenWorkOrder(i));
            }
        }

        //Check for status and delete
        /*We will assume that the job has been completed in Macola when the data doesn't exist in macolaData Container.
          Because macolaData is pulling from a view that is only querying open work orders.
         */
        for (OpenWorkOrder i : productionScheduleData){
            boolean exists = false;
            for(WorkOrder j : macolaData){
                if(i.getWorkOrderNum() == j.getWorkOrderNum()){
                    exists = true;
                }
            }

            if (exists == false){
                /*Work around, make a simple array, and add the index to the the array.
                Then go through and remove by index.*/
                listOfItemsToDelete.add(i);
            }
        }

        if (listOfItemsToDelete.size() > 0){
            productionScheduleData.removeAll(listOfItemsToDelete);
        }

        return productionScheduleData; //Return Open Work Objects that contain all the data from both
    }

    /*
    retrieveProductionScheduleData method accesses the database that the Production Schedule uses to save it's
    custom fields and then loads that data in to OpenWorkOrder objects.
     */
    private ObservableList<OpenWorkOrder> retrieveProductionScheduleData(){
        String connectionUrl = generateConnectionUrl(ProductionSchedule.productionScheduleConfig);
        ResultSet rs;
        ObservableList<OpenWorkOrder> productionScheduleData = FXCollections.observableArrayList();

        try (Connection con = DriverManager.getConnection(connectionUrl); Statement stmt = con.createStatement();) {
            String SQL = "SELECT * FROM [ProductionSchedule].[dbo].[Open_Work_Orders] ORDER BY production_line, scheduled_date, priority;";
            rs = stmt.executeQuery(SQL);

            while(rs.next()){
                //Load Macola Data into Work Order object for processing
                productionScheduleData.add(new OpenWorkOrder(rs.getDate("scheduled_date"),
                        rs.getString("production_line"), rs.getString("work_shift"),
                        rs.getInt("priority"), null, rs.getInt("work_order_num"),
                        null, 0, null, null,
                        null, null, 0, null, rs.getString("comment")));
            }
        }
        // Handle any errors that may have occurred.
        catch (SQLException e) {
            e.printStackTrace();
        }

        return productionScheduleData;
    }

    /*
    retrieveMacolaData method accesses the Macola server and loads the data into WorkOrder objects.
     */
    private static ObservableList<WorkOrder> retrieveMacolaData(){
        String connectionUrl = generateConnectionUrl("macolaconfig");
        ResultSet rs;
        ObservableList<WorkOrder> macolaData = FXCollections.observableArrayList();

        try (Connection con = DriverManager.getConnection(connectionUrl); Statement stmt = con.createStatement();) {
            String SQL = "SELECT * FROM [DATA_01].[dbo].[vPROD_OpenOrd_PPI] ORDER BY due_dt, ord_no;";
            rs = stmt.executeQuery(SQL);

            while(rs.next()){
                //Load Macola Data into Work Order object for processing
                macolaData.add(new WorkOrder(rs.getDate("due_dt"), rs.getInt("ord_no"),
                        trimString(rs.getString("cus_name")), rs.getInt("ord_qty"),
                        trimString(rs.getString("item_no")), trimString(rs.getString("item_desc_1")),
                        trimString(rs.getString("sub_item")), trimString(rs.getString("ovly_item")),
                        rs.getInt("unit_size"), trimString(rs.getString("user_def_fld_2"))));
            }
        }
        // Handle any errors that may have occurred.
        catch (SQLException e) {
            e.printStackTrace();
        }

        return macolaData;
    }

    /*
    Saves the OpenWorkOrder objects in memory to the production schedule database
     */
    public static void saveProductionScheduleData(){
        String connectionUrl = generateConnectionUrl(ProductionSchedule.productionScheduleConfig);

        try (Connection con = DriverManager.getConnection(connectionUrl); Statement stmt = con.createStatement();) {
            String SQL = "TRUNCATE TABLE [ProductionSchedule].[dbo].[Open_Work_Orders];";
            stmt.executeUpdate(SQL);

            for (OpenWorkOrder iterator : ProductionSchedule.getProdSchWorkOrders()) {
                    int ID = iterator.getWorkOrderNum();

                    Object schDate;
                    if (iterator.getSchDate() != null){
                        schDate = iterator.getSchDate();
                    } else {
                        schDate = null;
                    }

                    String productionLine = iterator.getProdLine();
                    String workShift = iterator.getWorkShift();
                    int prioritySeq = iterator.getPrioritySeq();
                    int salesOrder;

                    if (isNumeric(iterator.getSalesOrder())){
                        salesOrder = Integer.parseInt(iterator.getSalesOrder());
                    } else {
                        salesOrder = 0;
                    }

                    String itemSKU = iterator.getFinishedSKU();

                    Object dueDate;
                    if (iterator.getDueDate() != null){
                        dueDate = iterator.getDueDate();
                    } else {
                        dueDate = null;
                    }

                    String comment = iterator.getComment();

                    if (schDate != null && dueDate != null) {
                        SQL = "INSERT INTO [ProductionSchedule].[dbo].[Open_Work_Orders] VALUES (" + ID + ",'"
                                + schDate.toString() + "','" + productionLine + "','" + workShift + "','" + prioritySeq +
                                "','" + itemSKU + "','" + salesOrder + "','" + dueDate + "','" + comment + "');";
                    } else {
                        //This sets the default value for the schedule date to empty. It's much easier to see an
                        // unscheduled job if the production line and scheduled date is empty.
                        SQL = "INSERT INTO [ProductionSchedule].[dbo].[Open_Work_Orders] " +
                                "(work_order_num, production_line, work_shift, priority, item_sku, sales_order, due_date, comment) " +
                                "VALUES (" + ID + ", '" + productionLine + "','" + workShift + "','" + prioritySeq + "','" +
                                itemSKU + "','" + salesOrder + "','" + dueDate + "','" + comment + "');";
                    }

                    stmt.executeUpdate(SQL);
            }
        }
        // Handle any errors that may have occurred.
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static ObservableList<OpenWorkOrder> getProdSchWorkOrders() {
        return prodSchWorkOrders;
    }

    public static void setProdSchWorkOrders(ObservableList<OpenWorkOrder> argProdSchWorkOrders) {
        prodSchWorkOrders = argProdSchWorkOrders;
    }

    private static String trimString(String arg){
        if (arg != null){
            arg = arg.trim();
        }
        return arg;
    }

    public static void sortProductionSchedule(){
        //FXCollections.sort(prodSchWorkOrders, new workOrderSortingComparator());
        //FXCollections.reverse(prodSchWorkOrders);
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static void saveRowChanges(OpenWorkOrder selectWorkOrder) {
        String connectionUrl = generateConnectionUrl("productionscheduleconfig");

        try (Connection con = DriverManager.getConnection(connectionUrl); Statement stmt = con.createStatement();) {
            String SQL = "";

            int workOrderNum = selectWorkOrder.getWorkOrderNum();

            Object schDate;
            if (selectWorkOrder.getSchDate() != null) {
                schDate = selectWorkOrder.getSchDate();
            } else {
                schDate = null;
            }

            String productionLine = selectWorkOrder.getProdLine();
            String workShift = selectWorkOrder.getWorkShift();
            int prioritySeq = selectWorkOrder.getPrioritySeq();
            String comment = selectWorkOrder.getComment();

            if (schDate != null) {
                SQL = "UPDATE [ProductionSchedule].[dbo].[Open_Work_Orders]" +
                        "SET scheduled_date = '" + schDate + "', production_line = '" + productionLine
                        + "', work_shift = '" + workShift + "', priority = " + prioritySeq + ", comment = '" + comment + "'" +
                        "WHERE work_order_num = " + workOrderNum;
            } else {
                //This sets the default value for the schedule date to empty. It's much easier to see an
                // unscheduled job if the production line and scheduled date is empty.
                SQL = "UPDATE [ProductionSchedule].[dbo].[Open_Work_Orders]" +
                        "SET production_line = " + productionLine
                        + ", work_shift = " + workShift + ", priority = " + prioritySeq + ", comment = '" + comment + "'" +
                        "WHERE work_order_num = " + workOrderNum;
            }
            stmt.executeUpdate(SQL);
        }
        // Handle any errors that may have occurred.
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static String generateConnectionUrl(String nameOfConfig){
        String url = "";
        String dbName = "";
        String user = "";
        String password = "";

        Properties prop = new Properties();
        try (InputStream input = new FileInputStream("src/resources/" + nameOfConfig + ".properties")) {

            prop.load(input);
            url = prop.getProperty("db.url");
            dbName = prop.getProperty("db.dbName");
            user = prop.getProperty("db.user");
            password = prop.getProperty("db.password");
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return "jdbc:sqlserver://" + url + ";databaseName=" + dbName + ";user=" + user + ";password=" + password;
    }
}
